plugins {
    alias(libs.plugins.kotlinJvm)
}

group = "com.glapps.maifest"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    implementation(libs.firebase.admin)
    implementation(libs.kotlin.coroutines.core)
    implementation(libs.kotlin.coroutines.guava)

    testImplementation(libs.kotest.runner)
}

tasks.withType<Test>().configureEach {
    useJUnitPlatform()
}