package com.glapps.maifest

import com.glapps.maifest.data.CategoryWrapper
import com.glapps.maifest.data.ItemWrapper
import com.glapps.maifest.exception.DuplicateIdException
import com.glapps.maifest.extensions.await
import com.glapps.maifest.model.Identifiable
import com.glapps.maifest.model.ItemVariant
import com.google.firebase.cloud.FirestoreClient

object FireStoreUpload {
    private const val BASE_PATH = "Categories"
    private val db by lazy { FirestoreClient.getFirestore() }

    suspend fun uploadCategories(categories: List<CategoryWrapper>) {
        verifyUniqueIds(categories)
        resetDatabase()

        categories.forEach { (category, items) ->
            upload(BASE_PATH, category)

            items.forEach { (item, itemVariants) ->
                val itemPath = "$BASE_PATH/${category.id}/Items"
                upload(itemPath, item)

                itemVariants.forEach { variant ->
                    val variantPath = "$itemPath/${item.id}/ItemVariants"
                    upload(variantPath, variant)
                }
            }
        }
        println("Successfully uploaded ${categories.size} categories")
    }

    private suspend fun resetDatabase() {
        db.recursiveDelete(db.collection(BASE_PATH)).await()
        println("Performed database reset")
    }

    private fun verifyUniqueIds(categories: List<CategoryWrapper>) {
        val ids = categories.flattenIds()
        val duplicateIds = ids.groupingBy { it }.eachCount().filter { it.value > 1 }
        when (duplicateIds.isEmpty()) {
            true -> println("No duplicate ids found")
            false -> throw DuplicateIdException(duplicateIds)
        }
    }

    @JvmName("flattenCategoryIds")
    private fun List<CategoryWrapper>.flattenIds(): List<String> =
        flatMap { (category, items) -> items.flattenIds() + category.id }

    @JvmName("flattenItemIds")
    private fun List<ItemWrapper>.flattenIds(): List<String> =
        flatMap { (item, variants) -> variants.map(ItemVariant::id) + item.id }

    private suspend fun upload(path: String, element: Identifiable) =
        runCatching {
            db.collection(path).document(element.id).set(element).await()
        }.onFailure { exception ->
            error("Failed to add element $element to remote database due to ${exception.message}")
        }.onSuccess {
            println("Added element $element to remote database")
        }
}
