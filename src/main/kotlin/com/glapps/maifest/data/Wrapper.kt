package com.glapps.maifest.data

import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant
import com.glapps.maifest.model.Positioned

data class CategoryWrapper(
    val category: Category,
    val items: List<ItemWrapper>,
    override val position: Int = 0,
) : Positioned<CategoryWrapper> {
    override fun withPosition(position: Int) = copy(category = category.withPosition(position))
}

data class ItemWrapper(
    val item: Item,
    val itemVariants: List<ItemVariant>,
    override val position: Int = 0,
) : Positioned<ItemWrapper> {
    override fun withPosition(position: Int) = copy(item = item.withPosition(position))
}


