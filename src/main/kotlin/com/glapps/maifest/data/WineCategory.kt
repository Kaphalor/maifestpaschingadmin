package com.glapps.maifest.data

import com.glapps.maifest.extensions.positionedListOf
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant

val wineWrapper = CategoryWrapper(
    category = Category(id = "GgnBG2XUg1UromQAEzSOy", name = "Wein & Most"),
    items = positionedListOf(
        ItemWrapper(
            item = Item(id = "u0lhPpOTQevMnS254K_3k", name = "Most"),
            itemVariants = positionedListOf(
                ItemVariant(id = "cnlIuVGcch1kjZ97GHcEy", name = "Pur", price = 3.0, colorCode = "#ffff00"),
                ItemVariant(id = "MRxwWNK8mU2f80zFba2Kn", name = "G'spritzt", price = 3.0, colorCode = "#ffff00"),
            )
        ),
        ItemWrapper(
            item = Item(id = "pbAbzijA87yY2keuuEzGt", name = "Schankwein"),
            itemVariants = positionedListOf(
                ItemVariant(id = "yonDmDrcicPHTk4A0wVof", name = "¼ Schankwein", price = 4.0, colorCode = "#00b0f0"),
                ItemVariant(id = "hMBQXUQ1djgVHjrUSWt-Z", name = "¼ G'spritzt", price = 3.0, colorCode = "#ffff00"),
                ItemVariant(
                    id = "-nN5fa77bzmB07jQ-b9uG",
                    name = "½ Sommer G'spritzter",
                    price = 3.8,
                    colorCode = "#4c4c4c"
                ),
            )
        ),
        ItemWrapper(
            item = Item(id = "eGBrH_tbDSBB7IpiZpLWN", name = "Hütte"),
            itemVariants = positionedListOf(
                ItemVariant(id = "crfqfq3hBureHTe2mwwRO", name = "⅛ Weiß", price = 3.0, colorCode = "#ffff00"),
                ItemVariant(id = "PvLRgmPdWTconRsDOePIQ", name = "⅛ Rot", price = 3.0, colorCode = "#ffff00"),
                ItemVariant(id = "wQrYo-j0qH0J1UXUst_2T", name = "⅛ Frizzante", price = 3.5),
                ItemVariant(id = "iBCVkob0Ud2ubgSCWGiQx", name = "⅛ Frizzante Rosè", price = 3.5),
                ItemVariant(id = "UKcZE1ERbF8tqpCQ0tLbf", name = "Flasche Weiß", price = 16.0),
                ItemVariant(id = "zMWdgOrWYLgeU2zKGTPSm", name = "Flasche Rot", price = 16.0),
                ItemVariant(id = "GQ_Zgvyz_zsC2F6zkn6c0", name = "Flasche Frizzante", price = 18.0),
                ItemVariant(id = "bPHf1EVAUG8OSZKNyt85P", name = "Flasche Frizzante Rosè", price = 18.0),
            )
        ),
    )
)