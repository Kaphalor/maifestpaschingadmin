package com.glapps.maifest.data

import com.glapps.maifest.extensions.positionedListOf
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant

val beerWrapper = CategoryWrapper(
    category = Category(id = "BfXx6iQ1By6_yMqiMCuT2", name = "Bier"),
    items = positionedListOf(
        ItemWrapper(
            item = Item(id = "zn0YOSibT74UB6tq5AxwU", name = "Bier"),
            itemVariants = positionedListOf(
                ItemVariant(id = "ZB6We9wZn9gPbAChEntV7", name = "Halbe", price = 4.5, colorCode = "#ff0000"),
                ItemVariant(id = "1O92Hsqiezv4ppQ1wXZVV", name = "Seiterl", price = 3.8, colorCode = "#4c4c4c"),
            )
        ),
        ItemWrapper(
            item = Item(id = "v28kxuH2A8k2xe8_Dxyqn", name = "Radler"),
            itemVariants = positionedListOf(
                ItemVariant(id = "LKZea8uSvc2SnGnw3R3sZ", name = "Halbe", price = 4.5, colorCode = "#ff0000"),
                ItemVariant(id = "yS3-xMLmK_sKislAgs1D1", name = "Seiterl", price = 3.8, colorCode = "#4c4c4c"),
                ItemVariant(id = "xp9SkUqYkOR7nxF7Na6Iw", name = "Saurer Groß", price = 4.5, colorCode = "#ff0000"),
                ItemVariant(id = "t9q9M_BDiucHvRIxQTxPv", name = "Saurer Klein", price = 3.8, colorCode = "#4c4c4c"),
            )
        ),
    )
)