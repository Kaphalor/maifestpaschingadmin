package com.glapps.maifest.data

import com.glapps.maifest.extensions.positionedListOf
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant

val foodWrapper = CategoryWrapper(
    category = Category(id = "VcQ3nUHAgO09GYHDT3Avt", name = "Essen"),
    items = positionedListOf(
        ItemWrapper(
            item = Item(id = "cFcP0Q20qwPMmCTcEZFL7", name = "Grillhendl"),
            itemVariants = positionedListOf(
                ItemVariant(id = "F16WPrHdEof_Nk0xKiCMz", name = "½ Grillhendl", price = 11.5),
            )
        ),
        ItemWrapper(
            item = Item(id = "AupHbk5mGDKV88yM6vPbZ", name = "Bratwürstel"),
            itemVariants = positionedListOf(
                ItemVariant(id = "o0Bnih9ycG5vkHiXmabi1", name = "Mit Sauerkraut", price = 4.5, colorCode = "#ff0000"),
                ItemVariant(id = "FsM-gmMLNk7CHk6My-K07", name = "Mit Pommes", price = 6.0),
            )
        ),
        ItemWrapper(
            item = Item(id = "k8N0Zu5LaltO0E_HOoJmK", name = "Bosner"),
            itemVariants = positionedListOf(
                ItemVariant(id = "eb3rHyRGYVV9LbTR6_nHp", name = "Groß", price = 4.5, colorCode = "#ff0000"),
                ItemVariant(id = "BTs9kz5n3R8lCys3C3df0", name = "Klein", price = 4.0, colorCode = "#00b0f0"),
            )
        ),
        ItemWrapper(
            item = Item(id = "aCiDhaiQl35sozR5c80z4", name = "Pommes"),
            itemVariants = positionedListOf(
                ItemVariant(id = "OjFWy95JsG5TLs4--IadP", name = "Pommes", price = 3.0, colorCode = "#ffff00"),
            )
        ),
        ItemWrapper(
            item = Item(id = "L0ENK4SHqYqLLTz1Fp8Kq", name = "Mehlspeise"),
            itemVariants = positionedListOf(
                ItemVariant(id = "UnkXe7fBBXtXANXO4BxDS", name = "Mehlspeise", price = 3.0, colorCode = "#ffff00"),
            )
        ),
    )
)