package com.glapps.maifest.data

import com.glapps.maifest.extensions.positionedListOf
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant

val antiWrapper = CategoryWrapper(
    category = Category(id = "Beysl_WjnMvUMZ9Vs3Pin", name = "Anti"),
    items = positionedListOf(
        ItemWrapper(
            item = Item(id = "fDq1DfFRuVorvf6TtugFb", name = "Limo"),
            itemVariants = positionedListOf(
                ItemVariant(id = "4QEtjWh_2WIeStVhgOjUY", name = "½ Cola", price = 2.5, colorCode = "#92d050"),
                ItemVariant(id = "zFZL888_MFgVTOh5avsDZ", name = "½ Orange", price = 2.5, colorCode = "#92d050"),
                ItemVariant(id = "IQJePi6Ihs8NnXZwHFUYV", name = "½ Kräuter", price = 2.5, colorCode = "#92d050"),
                ItemVariant(id = "z5fahfhT8hg41XLG3zs8K", name = "⅓ Eistee", price = 2.5, colorCode = "#92d050"),
            )
        ),
        ItemWrapper(
            item = Item(id = "f7--Qlgak8ySqMhfY3J9M", name = "Limo G'spritzt"),
            itemVariants = positionedListOf(
                ItemVariant(id = "mnvYounpP2UCrl18RFGNV", name = "½ Cola", price = 2.5, colorCode = "#92d050"),
                ItemVariant(id = "MTImRzZKdLyLSeJVlR0S4", name = "½ Orange", price = 2.5, colorCode = "#92d050"),
                ItemVariant(id = "MH13eAcmleJYL6R22XSIv", name = "½ Kräuter", price = 2.5, colorCode = "#92d050"),
            )
        ),
        ItemWrapper(
            item = Item(id = "43aOphWQlulqOndpqldhZ", name = "Löschwasser"),
            itemVariants = positionedListOf(
                ItemVariant(id = "Lvm9ZxJz4_GbwSUJ-ZSRc", name = "½ Löschwasser", price = 1.5, colorCode = "#b2a0c7"),
            )
        ),
        ItemWrapper(
            item = Item(id = "fItrB5SU2oD_YhTcB_MFl", name = "Mineral"),
            itemVariants = positionedListOf(
                ItemVariant(id = "uvRP41qWvW55zNfXh5gPr", name = "⅓ Mineral", price = 2.5, colorCode = "#92d050"),
            )
        ),
        ItemWrapper(
            item = Item(id = "QeMsaShB-TD9kae0k1z_E", name = "Kaffee"),
            itemVariants = positionedListOf(
                ItemVariant(id = "K-_u-KCrKbQJjdjSUx8an", name = "Kaffee", price = 2.5, colorCode = "#92d050"),
            )
        ),
    )
)