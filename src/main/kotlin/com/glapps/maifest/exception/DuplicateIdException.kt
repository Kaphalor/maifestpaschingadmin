package com.glapps.maifest.exception

class DuplicateIdException(duplicateIds: Map<String, Int>) : Exception("Duplicate ids found: ${duplicateIds.keys}")