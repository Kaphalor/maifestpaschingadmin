package com.glapps.maifest.model

interface Positioned<T : Positioned<T>> {
    val position: Int

    fun withPosition(position: Int): T
}