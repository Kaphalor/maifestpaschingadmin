package com.glapps.maifest.model

data class Category(
    override val id: String,
    val name: String,
    override val position: Int = 0
) : Identifiable, Positioned<Category> {
    override fun withPosition(position: Int) = copy(position = position)
}