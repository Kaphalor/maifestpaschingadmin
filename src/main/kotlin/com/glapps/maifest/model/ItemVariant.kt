package com.glapps.maifest.model

data class ItemVariant(
    override val id: String,
    val name: String,
    val price: Double,
    override val position: Int = 0,
    val colorCode: String? = null,
) : Identifiable, Positioned<ItemVariant> {

    override fun withPosition(position: Int) = copy(position = position)

}