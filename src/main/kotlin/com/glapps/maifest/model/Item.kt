package com.glapps.maifest.model

data class Item(
    override val id: String,
    val name: String,
    override val position: Int = 0
) : Identifiable, Positioned<Item> {

    override fun withPosition(position: Int) = copy(position = position)

}
