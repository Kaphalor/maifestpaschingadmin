package com.glapps.maifest.model

interface Identifiable {
    val id: String
}