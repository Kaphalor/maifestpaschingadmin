package com.glapps.maifest

import com.google.auth.oauth2.GoogleCredentials
import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import java.io.FileInputStream


object FireBaseClient {
    fun login() {
        val serviceAccount = FileInputStream("dev/secrets.json")
        val options = FirebaseOptions.builder().setCredentials(GoogleCredentials.fromStream(serviceAccount)).build()

        FirebaseApp.initializeApp(options)
    }
}