package com.glapps.maifest.extensions

import com.glapps.maifest.model.Positioned

fun <T : Positioned<T>> positionedListOf(vararg elements: T): List<T> = buildList {
    elements.forEachIndexed { index, element ->
        add(element.withPosition(index))
    }
}