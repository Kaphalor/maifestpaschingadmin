package com.glapps.maifest.extensions

import com.google.api.core.ApiFuture
import com.google.api.core.ApiFutureToListenableFuture
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.guava.asDeferred

fun <T> ApiFuture<T>.asDeferred(): Deferred<T> =
    ApiFutureToListenableFuture(this).asDeferred()

suspend fun <T> ApiFuture<T>.await(): T = asDeferred().await()