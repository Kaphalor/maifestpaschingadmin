package com.glapps.maifest

import com.glapps.maifest.data.antiWrapper
import com.glapps.maifest.data.beerWrapper
import com.glapps.maifest.data.foodWrapper
import com.glapps.maifest.data.wineWrapper
import com.glapps.maifest.extensions.positionedListOf
import kotlinx.coroutines.runBlocking

fun main() = runBlocking {
    FireBaseClient.login()

    FireStoreUpload.uploadCategories(
        positionedListOf(beerWrapper, wineWrapper, antiWrapper, foodWrapper)
    )
}