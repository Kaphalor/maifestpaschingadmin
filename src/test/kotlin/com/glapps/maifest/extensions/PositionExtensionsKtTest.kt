package com.glapps.maifest.extensions

import com.glapps.maifest.data.CategoryWrapper
import com.glapps.maifest.data.ItemWrapper
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.collections.shouldContainInOrder


class PositionExtensionsTest : ShouldSpec({

    should("create list of Categories with correct position") {
        val list = positionedListOf(
            CategoryWrapper(Category(id = "0", name = "0"), items = emptyList(), position = -5),
            CategoryWrapper(Category(id = "1", name = "1"), items = emptyList(), position = 0),
        )

        list.map(CategoryWrapper::category).shouldContainInOrder(
            Category(id = "0", name = "0", position = 0),
            Category(id = "1", name = "1", position = 1),
        )
    }

    should("create list of Items with correct position") {
        val list = positionedListOf(
            ItemWrapper(Item(id = "0", name = "0"), itemVariants = emptyList(), position = -5),
            ItemWrapper(Item(id = "1", name = "1"), itemVariants = emptyList(), position = 0),
        )

        list.map(ItemWrapper::item).shouldContainInOrder(
            Item(id = "0", name = "0", position = 0),
            Item(id = "1", name = "1", position = 1),
        )
    }

    should("create list of ItemVariants with correct position") {
        val list = positionedListOf(
            ItemVariant(id = "0", name = "0", price = 0.0, position = -5),
            ItemVariant(id = "1", name = "1", price = 1.1, position = 0),
        )

        list.shouldContainInOrder(
            ItemVariant(id = "0", name = "0", price = 0.0, position = 0),
            ItemVariant(id = "1", name = "1", price = 1.1, position = 1),
        )
    }
})