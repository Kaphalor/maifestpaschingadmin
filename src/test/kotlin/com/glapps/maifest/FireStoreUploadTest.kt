package com.glapps.maifest

import com.glapps.maifest.data.CategoryWrapper
import com.glapps.maifest.data.ItemWrapper
import com.glapps.maifest.exception.DuplicateIdException
import com.glapps.maifest.model.Category
import com.glapps.maifest.model.Item
import com.glapps.maifest.model.ItemVariant
import io.kotest.assertions.throwables.shouldThrow
import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe

class FireStoreUploadTest : ShouldSpec({

    should("throw exception if categories have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"), items = emptyList()),
            CategoryWrapper(category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "1"), items = emptyList()),
            CategoryWrapper(category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "2"), items = emptyList()),
            CategoryWrapper(category = Category(id = "kG8J8WteDd67orKK1UE-3", name = "3"), items = emptyList()),
            CategoryWrapper(category = Category(id = "kG8J8WteDd67orKK1UE-3", name = "4"), items = emptyList()),
            CategoryWrapper(category = Category(id = "-nZ2HV2Aax0xMkuUPHvux", name = "5"), items = emptyList()),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [LXW-r2sMzNdL1uLUraBXn, kG8J8WteDd67orKK1UE-3]"
    }

    should("throw exception if item and category have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(
                category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"),
                items = listOf(
                    ItemWrapper(item = Item(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"), itemVariants = emptyList())
                )
            ),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [LXW-r2sMzNdL1uLUraBXn]"
    }

    should("throw exception if items have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(
                category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"),
                items = listOf(
                    ItemWrapper(item = Item(id = "RUb5ibeF3f4964EckXSXl", name = "0"), itemVariants = emptyList()),
                    ItemWrapper(item = Item(id = "RUb5ibeF3f4964EckXSXl", name = "1"), itemVariants = emptyList()),
                )
            ),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [RUb5ibeF3f4964EckXSXl]"
    }

    should("throw exception if item variant and category have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(
                category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"),
                items = listOf(
                    ItemWrapper(
                        item = Item(id = "RUb5ibeF3f4964EckXSXl", name = "0"),
                        itemVariants = listOf(
                            ItemVariant(id = "LXW-r2sMzNdL1uLUraBXn", name = "0", price = 0.0)
                        )
                    ),
                )
            ),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [LXW-r2sMzNdL1uLUraBXn]"
    }

    should("throw exception if item variant and item have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(
                category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"),
                items = listOf(
                    ItemWrapper(
                        item = Item(id = "RUb5ibeF3f4964EckXSXl", name = "0"),
                        itemVariants = listOf(
                            ItemVariant(id = "RUb5ibeF3f4964EckXSXl", name = "0", price = 0.0)
                        )
                    ),
                )
            ),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [RUb5ibeF3f4964EckXSXl]"
    }

    should("throw exception if two items have duplicate ids") {
        val categories = listOf(
            CategoryWrapper(
                category = Category(id = "LXW-r2sMzNdL1uLUraBXn", name = "0"),
                items = listOf(
                    ItemWrapper(
                        item = Item(id = "RUb5ibeF3f4964EckXSXl", name = "0"),
                        itemVariants = listOf(
                            ItemVariant(id = "Q9HMKwjvXueFycSwltXgM", name = "0", price = 0.0),
                            ItemVariant(id = "Q9HMKwjvXueFycSwltXgM", name = "1", price = 1.0),
                        )
                    ),
                )
            ),
        )

        shouldThrow<DuplicateIdException> {
            FireStoreUpload.uploadCategories(categories)
        }.message shouldBe "Duplicate ids found: [Q9HMKwjvXueFycSwltXgM]"
    }
})
